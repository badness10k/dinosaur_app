class DinosaursController < ApplicationController
  before_action :set_dinosaur, only: [:show, :edit, :update, :destroy]

  # GET /dinosaurs
  # GET /dinosaurs.json
  def index
    @dinosaurs = params[:cage_id].nil? ? Dinosaur.all : Dinosaur.where(cage_id: params[:cage_id])
  end

  # GET /dinosaurs/1
  # GET /dinosaurs/1.json
  def show
  end

  # GET /dinosaurs/new
  def new
    @dinosaur = Dinosaur.new
  end

  # GET /dinosaurs/1/edit
  def edit
  end

  # POST /dinosaurs
  # POST /dinosaurs.json
  def create
    @dinosaur = Dinosaur.new(dinosaur_params)

    respond_to do |format|
      if Cage.find_by(id: @dinosaur.cage_id).nil?
        # cage does not exist
        @dinosaur.errors.add(:cage_id, "Cage does not exist!")
        format.json { render json: @dinosaur.errors, status: :unprocessable_entity }

      elsif !Cage.find(@dinosaur.cage_id).has_power?
        # don't add dinosaur if cage doesn't have power
        @dinosaur.errors.add(:cage_id, "Cannot add dinosaur to a cage without power!")
        format.json { render json: @dinosaur.errors, status: :unprocessable_entity }

      elsif @dinosaur.Carnivore? && @dinosaur.cage.dinosaurs.Herbivore.any? ||
            @dinosaur.Herbivore? && @dinosaur.cage.dinosaurs.Carnivore.any?
        # don't mix carnivores and herbivores
        @dinosaur.errors.add(:cage_id, "Don't mix carnivores and herbivores!")
        format.json { render json: @dinosaur.errors, status: :unprocessable_entity }

      elsif @dinosaur.Carnivore? && @dinosaur.cage.dinosaurs.where.not(species: Dinosaur.species[@dinosaur.species]).any?
        # don't mix carnivores with different species
        @dinosaur.errors.add(:cage_id, "Cannot add carnivore to cage with species different than itself!")
        format.json { render json: @dinosaur.errors, status: :unprocessable_entity }

      elsif @dinosaur.cage.current >= @dinosaur.cage.capacity
        # don't add to full cage
        @dinosaur.errors.add(:cage_id, "Cannot add to full cage!")
        format.json { render json: @dinosaur.errors, status: :unprocessable_entity }

      elsif @dinosaur.save
        format.html { redirect_to @dinosaur, notice: 'Dinosaur was successfully created.' }
        format.json { render :show, status: :created, location: @dinosaur }
      else
        format.html { render :new }
        format.json { render json: @dinosaur.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /dinosaurs/1
  # PATCH/PUT /dinosaurs/1.json
  def update
    dest_cage = Cage.find_by(id: dinosaur_params[:cage_id])

    respond_to do |format|
      if dest_cage.nil?
        # cage does not exist
        @dinosaur.errors.add(:cage_id, "Cage does not exist!")
        format.json { render json: @dinosaur.errors, status: :unprocessable_entity }

      elsif !dest_cage.has_power?
        # don't add dinosaur if cage doesn't have power
        @dinosaur.errors.add(:cage_id, "Cannot add dinosaur to a cage without power!")
        format.json { render json: @dinosaur.errors, status: :unprocessable_entity }

      elsif @dinosaur.Carnivore? && dest_cage.dinosaurs.Herbivore.any? ||
        @dinosaur.Herbivore? && dest_cage.dinosaurs.Carnivore.any?
        # don't mix carnivores and herbivores
        @dinosaur.errors.add(:cage_id, "Don't mix carnivores and herbivores!")
        format.json { render json: @dinosaur.errors, status: :unprocessable_entity }

      elsif @dinosaur.Carnivore? && dest_cage.dinosaurs.where.not(species: Dinosaur.species[@dinosaur.species]).any?
        # don't mix carnivores with different species
        @dinosaur.errors.add(:cage_id, "Cannot add carnivore to cage with species different than itself!")
        format.json { render json: @dinosaur.errors, status: :unprocessable_entity }

      elsif dest_cage.current + 1 >= dest_cage.capacity
        # don't add to full cage
        @dinosaur.errors.add(:cage_id, "Cannot add to full cage!")
        format.json { render json: @dinosaur.errors, status: :unprocessable_entity }

      elsif @dinosaur.update(dinosaur_params)
        format.html { redirect_to @dinosaur, notice: 'Dinosaur was successfully updated.' }
        format.json { render :show, status: :ok, location: @dinosaur }
      else
        format.html { render :edit }
        format.json { render json: @dinosaur.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dinosaurs/1
  # DELETE /dinosaurs/1.json
  def destroy
    @dinosaur.destroy
    respond_to do |format|
      format.html { redirect_to dinosaurs_url, notice: 'Dinosaur was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_dinosaur
    @dinosaur = Dinosaur.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def dinosaur_params
    params.require(:dinosaur).permit(:cage_id, :name, :species, :diet)
  end

end
