json.array!(@cages) do |cage|
  json.extract! cage, :id, :capacity, :current, :has_power
  json.url cage_url(cage, format: :json)
end
