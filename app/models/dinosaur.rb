class Dinosaur < ActiveRecord::Base
  belongs_to :cage

  validates :cage_id, presence: true
  validates :name, presence: true
  validates :species, presence: true

  enum species: [ :Tyrannosaurus, :Velociraptor, :Spinosaurus, :Megalosaurus, :Brachiosaurus, :Stegosaurus, :Ankylosaurus, :Triceratops ]
  enum diet: [ :Herbivore, :Carnivore ]

  after_initialize :set_defaults

  def set_defaults
    # Set diet based on species
    if self.species.in? [ "Tyrannosaurus", "Velociraptor", "Spinosaurus", "Megalosaurus" ]
        self.diet = :Carnivore
    else
      self.diet = :Herbivore
    end
  end
end
