class Cage < ActiveRecord::Base
  has_many :dinosaurs

  validates :capacity, presence: true
  validates :current, presence: true
	validates :has_power, inclusion: { in: [true, false] }

  validate :current_less_than_capacity

  after_initialize :update_current

  def current_less_than_capacity
    if self.current > self.capacity
      errors.add(:current, "can't be greater than capacity: #{self.capacity}")
    end
  end

  def update_current
    # update current with dinosaur count
    self.update(current: self.dinosaurs.count);
  end
end
