## README

Sample dinosaur app for insightpool dev test.

TODO:

* ~~Cages must have a maximum capacity for how many dinosaurs it can hold.~~
* ~~Cages know how many dinosaurs are contained.~~
* ~~Cages have a power status of ACTIVE or DOWN.~~
* ~~Cages cannot be powered off if they contain dinosaurs.~~
* ~~Dinosaurs cannot be moved into a cage that is powered down.~~
* ~~Each dinosaur must have a name.~~
* ~~Each dinosaur must have a species (See enumerated list below, feel free to add others).~~
* ~~Each dinosaur is considered an herbivore or a carnivore, depending on its species.~~
* ~~Herbivores cannot be in the same cage as carnivores.~~
* ~~Carnivores can only be in a cage with other dinosaurs of the same species.~~
* ~~Must be able to query a listing of dinosaurs in a specific cage.~~
* When querying dinosaurs or cages they should be filterable on their attributes (Cages on their power status and dinosaurs on species).

### Notes:
I chose to use Postman for the testing, as it was all to easy to build requests and add them to a test collection and run them.

The filtering by cage power status is not working.

If this application was to be run in a concurrent environment, I would use a different web server instead of `WEBrick`. Probably `puma`. Also, I would use a database with concurrency control methods, like `PostgreSQL`. 

During this project I realized Dennis Nedry's last name is an anagram for 'nerdy'.
