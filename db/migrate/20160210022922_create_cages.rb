class CreateCages < ActiveRecord::Migration
  def change
    create_table :cages do |t|
      t.integer :capacity
      t.integer :current
      t.boolean :has_power

      t.timestamps null: false
    end
  end
end
