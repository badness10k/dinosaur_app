class CreateDinosaurs < ActiveRecord::Migration
  def change
    create_table :dinosaurs do |t|
      t.integer :cage_id
      t.string :name
      t.integer :species
      t.integer :diet

      t.timestamps null: false
    end
  end
end
